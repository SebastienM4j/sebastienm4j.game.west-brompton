#!/usr/bin/env node

const [,, word, result] = process.argv;

if(!word || word.length == 0) {
    console.log("A word (enigme result) is required in first arg");
    process.exit(10);
}
if(!result || result.length == 0) {
    console.log("A result (mini-crypto result) is required in second arg");
    process.exit(20);
}
if(word.length != result.length) {
    console.log("The word and result have not the same size");
    process.exit(30);
}


const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
const wordLetters = word.toUpperCase().split("");
const resultLetters = result.toUpperCase().split("");


function gap(wordLetter, index)
{
    resultLetter = resultLetters[index];

    indexWordLetter = alphabet.indexOf(wordLetter);
    indexAlpha = indexWordLetter;
    indexResultLetter = -1;
    while(indexResultLetter < 0) {
        if(alphabet[indexAlpha] === resultLetter) {
            indexResultLetter = indexAlpha;
        } else {
            indexAlpha++;
        }
    }

    return indexResultLetter-indexWordLetter;
}

crypto = wordLetters.map((wl, i) => gap(wl, i));


console.log(`${word.toUpperCase()} > ${crypto.join(" ")} > ${result.toUpperCase()}`);