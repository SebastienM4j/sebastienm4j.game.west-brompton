#!/usr/bin/env node

// Usage :
//
// ./minicrypto-decrypt.js MAGLOIRE 3,7,2,0,20,7,24,0
// ./minicrypto-decrypt.js ONOFRIO 4,7,11,21,22,5,16
// ./minicrypto-decrypt.js LAURORE 7,8,18,23,25,22,0
// ./minicrypto-decrypt.js PERSAN 23,0,2,25,4,5
// ./minicrypto-decrypt.js GODIVA 9,0,10,18,18,19
// ./minicrypto-decrypt.js MAYDAY 3,14,21,1,0,22

const [,, word, code] = process.argv;

if(!word || word.length == 0) {
    console.log("A word (enigme result) is required in first arg");
    process.exit(10);
}
if(!code || code.length == 0) {
    console.log("Code (mini-crypto code) is required in second arg");
    process.exit(20);
}
const lettersCode = code.split(",").map(Number);
if(word.length != lettersCode.length) {
    console.log("The word and code have not the same size");
    process.exit(30);
}

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
const wordLetters = word.toUpperCase().split("");


function gap(wordLetter, lettersCode, index)
{
    indexWordLetter = alphabet.indexOf(wordLetter);
    indexResultLetter = indexWordLetter+lettersCode[index];
    
    return alphabet[indexResultLetter];
}

crypto = wordLetters.map((wl, i) => gap(wl, lettersCode, i));


console.log(`${word.toUpperCase()} > ${code} > ${crypto.join("")}`);