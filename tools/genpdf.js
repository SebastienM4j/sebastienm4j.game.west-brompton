#!/usr/bin/env node

const puppeteer = require('puppeteer')


async function genPdf(fileName, format, landscape)
{
    const browser = await puppeteer.launch({ headless: true });
    
    const page = await browser.newPage();
    await page.goto(`http://localhost:3000/${fileName}.html`, { waitUntil: 'load' });
    await sleep(3000); // wait for the BrowserSync panel...
    
    // FIXME problèmes de rendu (décallages) impactant sur certaines énigmes
    await page.pdf({
        format: format,
        landscape: landscape,
        printBackground: true,
        preferCSSPageSize: true,
        path: `${__dirname}/../dist/${fileName}.pdf`
    });

    await browser.close();
}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}


[
    'briefing',
    'enigme_bermude',
    'enigme_grimm',
    'enigme_liberation',
    'enigme_minicrytos',
    'enigme_paon',
    'enigme_poeme',
    'enigme_unionjack',
    'teasing',
    'tresor'
].forEach((fileName) => genPdf(fileName, 'A4', false));

genPdf('enigme_gps', 'A5', true);