#!/usr/bin/env node

const childProcess = require('child_process');
const scriptPath = `${__dirname}/minicrypto.js`;

const [,, ...args] = process.argv;

if(args.length % 2 !== 0) {
    console.log("There must be an even number of arguments");
    process.exit(1);
}


function minicrypto(word, result, callback)
{
    var invoked = false;
    var process = childProcess.fork(scriptPath, [word, result]);

    process.on('error', function(err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    process.on('exit', function(code) {
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        callback(err);
    });
}

for(var i=0; i<args.length; i=i+2) {
    minicrypto(args[i], args[i+1], err => {
        if(err) {
            console.log(`Error for mini-crypto ${args[i]} > ${args[i+1]} : ${err}`);
        }
    });
}