West Brompton
=============

PDF des énigmes de la 2ème saison de chasse aux trésors Cityzen.


Environnement technique
-----------------------

* [NodeJS](https://nodejs.org)
* [Npm](https://www.npmjs.com)
* [Gulp](https://gulpjs.com)
* [Paper CSS](https://www.npmjs.com/package/paper-css)


Développement
-------------

### Installation

* Installer [NodeJS](https://nodejs.org) et [Npm](https://www.npmjs.com)
* Installer les dépendances `npm install`
* Lancer avec `gulp`

Tools
-----

Pour le calcul d'un mini-crypto :

```shell
$ ./tools/minicrypto.js {mot} {resultat}
```

Ou pour une série de mini-cryptos :

```shell
$ ./tools/minicrypto.js {mot} {resultat} {mot} {resultat} ...
```
