var gulp = require('gulp');
var rename = require("gulp-rename");
var browserSync = require('browser-sync').create();

var fail = false;

var onError = function(err)
{
  console.log(err.toString());
  this.emit('end');
  if(fail) {
    process.exit(1);
  }
};

gulp.task('ci', function() {
  fail = true;
});

gulp.task('html', function() {
  gulp.src('src/*.html')
      .pipe(gulp.dest('public/'));
});

gulp.task('images', function() {
  gulp.src('src/img/*')
      .pipe(gulp.dest('public/img/'));
});

gulp.task('fonts', function() {
  gulp.src('src/font/*')
      .pipe(gulp.dest('public/font/'));
});

gulp.task('css', function() {
  gulp.src('src/css/*')
      .pipe(gulp.dest('public/css/'));
});

gulp.task('vendor', function() {
  gulp.src(['node_modules/paper-css/paper.min.css'])
      .pipe(gulp.dest('public/vendor/paper-css/'));
});

gulp.task('browser-sync', function () {
  'use strict';
  browserSync.init({
    server: {
      baseDir: './public/'
    }
  });
});

gulp.task('watch', function() {
  gulp.watch('src/img/*', ['images']);
  gulp.watch('src/font/*', ['fonts']);
  gulp.watch('src/css/*.css', ['css']);
  gulp.watch('src/*.html', ['html']);
  gulp.watch('src/*.html', browserSync.reload);
});

gulp.task('build', ['ci', 'html', 'images', 'fonts', 'css', 'vendor']);

gulp.task('default', ['html', 'images', 'fonts', 'css', 'vendor', 'browser-sync', 'watch']);
